//
//  PowersetCalculator.m
//  PowersetCalculator
//
//  Created by ilir iliraga on 21/10/14.
//  Copyright (c) 2014 ilir iliraga. All rights reserved.
//

#import "PowersetCalculator.h"

@implementation PowersetCalculator

/**
 * @abstract calculates power set from specified input
 *
 * @param input input as NSArray
 *
 * @returns the powerset as NSArray Reference
 */
- (NSArray *)powerSetFrom:(NSArray *)input {
    // cache length because we access it a couple of times
    // in the following code
    NSInteger length = [input count];
    
    if (length == 0) {
        // when length of array is zero
        // this means is an empty set aka 'leere menge', so just
        // retrieve an array containing that empty set jehaau
        return [NSArray arrayWithObject:[[NSArray alloc] init]];
    }
    
    // get an object from the array and the array without that object
    // get the last object from the array
    id lastObject = [input lastObject];
    
    // .. and calculate a new array without the last object
    // we use normal ranges for that
    NSArray *strippedArray = [input subarrayWithRange:NSMakeRange(0,length-1)];

    // calculate the powerset of the new array
    // without the last object... we use recursion for that step
    NSArray *strippedArrayPowerset = [self powerSetFrom:strippedArray];

    // create the result array
    NSMutableArray *result = [NSMutableArray arrayWithArray:strippedArrayPowerset];
    
    // add the object which was previously removed to every element of the result
    // so we include it in each of them
    for (NSArray *lessOneElement in strippedArrayPowerset) {
        NSArray* powersetItem = [lessOneElement arrayByAddingObject:lastObject];
        
        // add to result list
        [result addObject:powersetItem];
    }
    
    // embed the whole powerset into another array
    return [NSArray arrayWithArray:result];
}

/**
 * @abstract calculates power set from specified input and formats to readable string
 *
 * @param input input as NSArray
 *
 * @returns the powerset as formatted NSArray-Instance
 */
-(NSString*)powerSetFormatted:(NSArray *)input{
    // calculate the power set
    NSArray* powerSet = [self powerSetFrom:input];
    
    // then start to format by replacing occurences one after another
    // first remove the newlines
    NSString *desc = [[powerSet description] stringByReplacingOccurrencesOfString:@"\n" withString:@""];

    // then the empty spaces
    return [desc stringByReplacingOccurrencesOfString:@" " withString:@""];
}
@end
