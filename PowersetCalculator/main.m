//
//  main.m
//  PowersetCalculator
//
//  Created by ilir iliraga on 21/10/14.
//  Copyright (c) 2014 ilir iliraga. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "PowersetCalculator.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        PowersetCalculator* calculator = [[PowersetCalculator alloc] init];
        
        // go through some testcases
        NSLog(@"%@", [calculator powerSetFormatted:[NSArray arrayWithObjects:@1, @3, nil]]);
//        NSLog(@"%@", [calculator powerSetFormatted:[[NSArray alloc] init]]);
//        NSLog(@"%@", [calculator powerSetFormatted:[NSArray arrayWithObjects:@1, @2, @3, nil]]);
    }
    return 0;
}
