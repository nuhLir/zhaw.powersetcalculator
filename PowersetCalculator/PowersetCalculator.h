//
//  PowersetCalculator.h
//  PowersetCalculator
//
//  Created by ilir iliraga on 21/10/14.
//  Copyright (c) 2014 ilir iliraga. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PowersetCalculator : NSObject
/**
 * @abstract calculates power set from specified input
 *
 * @param input input as NSArray
 *
 * @returns the powerset as NSArray Reference
 */
-(NSArray*)powerSetFrom:(NSArray *)input;

/**
 * @abstract calculates power set from specified input and formats to readable string
 *
 * @param input input as NSArray
 *
 * @returns the powerset as formatted NSArray-Instance
 */
-(NSString*)powerSetFormatted:(NSArray*)input;
@end
